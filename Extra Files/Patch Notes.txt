

v6.4.3.1



Addition: Added the following frames to rebar block

		rebarFramePlate,rebarFramePole,rebarFrameQuarter,rebarFrameHalf,rebarFramePillar50 (No localization yet)

Addition: Mortar to cobblestone recipes

Addition: Looging for Return Mods error

Change: Set server setting to default to 0 for POI Multiplier to help curb spawns
Change: Removed debugging code and log lines for zombie spawning air drops, biomes and night wights
Change: Flagstone must now dry like concrete
Change: Treasure zombies run faster
Change: MaxSpawnedZombies Options lowered to max of 140 in settings

Fix: Night Terror Hands Not registering properly



v6.4.3.2


Change: Jars no longer reuturn on drinks
Change: Thinned trees out a bit in the world
Change: Reduced Lead slightly on surfaces

Fix: Added missing volume crafts for Boats, Steel Tools and Kukri
Fix: Proper unlock info for steel tools

Removal: Hive Houses. There files are still present so games wont crash but new games and maps will no longer have them Significant frame drops in them. Nitrogen and RWG files updated

Removal: Removed following dll modules.
		BooksReadOnce
		ReturnModsonBreak
		



v6.4.3.3


Addition: Lighter version of no hope mall
Addition: Reworked zyncosa army poi
Addition: New phone quest
Addition: Craftable dog house
Addition: Dogs. just deco for now you have to defend but will expand
Addition: Surgeon zombie
Addition: 6 maps pregenerated using Nitrogen Default Settings tweaked. 3 4k and 3 8k

Change: Lowered max level to 200
Change: Stoves and ovens added to schematic
Change: You no longer get beack any jars or cans
Change: Leather armor moved to armor bench
Change: Mortar moved to pwb and recipe tweaked
Change: First aid case storage lowered

Fix: Removed zombie nurse template
Fix: Box truck schematic craft now includes volumes
Fix: Hoe scehmatic added t research desk
Fix: Steel tool heads should now craft properly
Fix: Some item descriptions cleaned up
Fix: Stag error fixed

Removal: Herd zombies removed from spawning
		



v6.4.3.4



Addition: Moved several recipes to variant blocks liek vanilla (Fridge Block, Rug Block, Statue Block) etc
Addition: The Heinekevirus!
Addition: Loot pile models as storage to decor table
Addition: Dust 2 death lighting and particles
Addition: 4k and 6k friendly RH prefab file for Nitrogen courtesy of Sinder
Addition: Irrigation Pipes now show fill info when you hover on them

Balance: Poi pallet harvest resources reduced
Balance: Trader will now sell items for more tellers
Balance: Increased ink in loot
Balance: Decreased attic treasures

Change: Poi pallets can now be looted of its resource
Change: All loot containers in the world can now be looted whether opened or closed. All containers have an equal chance of loot or nothing in them. Excluded are safes, treasure, piles and weapons bags.
Change: Tweaked zombie ai to target players before npcs

Fix: Wookies zombie balances. lowered max zombie setting to hardcap at 100, wh max count removed, poi multiplier maxes at x3
Fix: Steel shovel can now be scrapped
Fix: Stone Arrowheads no longer scrap
Fix: Artisan Lab Categories should now show
Fix: Phone can sometimes null due to improper reset count
Fix: Boats now consume fuel
Fix: Revolver should craft properly

Removal: Desert temple, bunker and school k6 pois for performance or other reasons


		



v6.4.3.5



Addition: More variant blocks added for pipes, christmas lights.

Change: Lowered crucible to perk 4 of advanced engineering
Change: Lowered blueberry to level 2 of living off the land

Removal: Dog companions due to respawn glitch. Will be enabled in future patch
Removal: All loot container mechanic in pois due to storage exploit. will return in future release
Removal: Heinekevirus due to nulling buff condition.


		



v6.4.3.6


Balance: Bone Knife and Hunting Knife now do more damage
Balance: Sell price of polymer lowered
Balance: Hatchet now scraps for less iron

Fix: Wookie Quest DLL. Quests should no longer reset on turn in to trader (empy quest list)
Fix: Carbine should now damage
Fix: Terror Experiment Hand Length
Fix: Attic Chest boundary box
Fix: Added second sub biome of copper to wasteland


		



v6.4.3.7


Change: Head Damage Modifiers now added to Blade and Blunt Action Skill
Change: Removed double stamina loss stats from electric weapons, brawler and javelin action skill

Fix: Horde night should now last way longer
Fix: Correct glass used for crafting stove recipe






v6.4.3



Addition: Added the following frames to rebar block

		rebarFramePlate,rebarFramePole,rebarFrameQuarter,rebarFrameHalf,rebarFramePillar50 (No localization yet)

Addition: Mortar to cobblestone recipes
Addition: Lighter version of no hope mall
Addition: Reworked zyncosa army poi
Addition: New phone quest
Addition: 3 maps pregenerated using Nitrogen Default Settings tweaked. 1 4k 1 6k and 1 8k
Addition: Moved several recipes to variant blocks like vanilla (Fridge Block, Rug Block, Statue Block, Pipe Block, Casket Block, Tombstone Block, Christmas Block) etc
Addition: Loot pile models as storage to decor table
Addition: Dust 2 death lighting and particles
Addition: 4k and 6k friendly RH prefab file for Nitrogen courtesy of Sinder
Addition: More variant blocks added for pipes, christmas lights.

Balance: Poi pallet harvest resources reduced
Balance: Trader will now sell items for more tellers
Balance: Increased ink in loot
Balance: Decreased attic treasures
Balance: Bone Knife and Hunting Knife now do more damage
Balance: Sell price of polymer lowered
Balance: Hatchet now scraps for less iron
Balance: Bone Knife and Hunting Knife now do more damage
Balance: Sell price of polymer lowered
Balance: Hatchet now scraps for less iron

Change: Lowered crucible to perk 4 of advanced engineering
Change: Lowered blueberry to level 2 of living off the land
Change: Lowered max level to 200
Change: Stoves and ovens added to schematic
Change: You no longer get beack any jars or cans
Change: Leather armor moved to armor bench
Change: Mortar moved to pwb and recipe tweaked
Change: First aid case storage lowered
Change: Set server setting to default to 0 for POI Multiplier to help curb spawns
Change: Removed debugging code and log lines for zombie spawning air drops, biomes and night wights
Change: Treasure zombies run faster
Change: MaxSpawnedZombies Options lowered to max of 140 in settings
Change: Thinned trees out a bit in the world
Change: Reduced Lead slightly on surfaces
Change: Poi pallets can now be looted of its resource
Change: Tweaked zombie ai to target players before npcs
Change: Lowered crucible to perk 4 of advanced engineering
Change: Lowered blueberry to level 2 of living off the land
Change: Head Damage Modifiers now added to Blade and Blunt Action Skill
Change: Removed double stamina loss stats from electric weapons, brawler and javelin action skill

Fix: Night Terror Hands Not registering properly
Fix: Added missing volume crafts for Boats, Steel Tools and Kukri
Fix: Proper unlock info for steel tools
Fix: Removed zombie nurse template
Fix: Box truck schematic craft now includes volumes
Fix: Hoe scehmatic added to research desk
Fix: Steel tool heads should now craft properly
Fix: Some item descriptions cleaned up
Fix: Stag error fixed
Fix: Wookies zombie balances. lowered max zombie setting to hardcap at 100, wh max count removed, poi multiplier maxes at x3
Fix: Steel shovel can now be scrapped
Fix: Artisan Lab Categories should now show
Fix: Stone Arrowheads no longer scrap
Fix: Phone can sometimes null due to improper reset count
Fix: Revolver should craft properly
Fix: Wookie Quest DLL. Quests should no longer reset on turn in to trader (empy quest list)
Fix: Carbine should now damage
Fix: Terror Experiment Hand Length
Fix: Attic Chest boundary box
Fix: Added second sub biome of copper to wasteland
Fix: Wookie Quest DLL. Quests should no longer reset on turn in to trader (empy quest list)
Fix: Carbine should now damage
Fix: Terror Experiment Hand Length
Fix: Attic Chest boundary box
Fix: Added second sub biome of copper to wasteland
Fix: Horde night should now last way longer
Fix: Correct glass used for crafting stove recipe

Removal: Desert temple, bunker and school k6 pois for performance or other reasons
Removal: Herd zombies removed from spawning
Removal: Hive Houses. There files are still present so games wont crash but new games and maps will no longer have them Significant frame drops in them. Nitrogen and RWG files updated

Removal: Removed following dll modules.
		BooksReadOnce
		ReturnModsonBreak





		
