﻿Key,Source,Context,English
emptyLargePetCage,items,Tool,Dog Cage (Empty),,,,,
emptyLargePetCageDesc,items,Tool,"This dog cage can be placed outside your base and with a little dog food you should attract a brand new pet. But make sure to keep them safe. They are not equipped to handle this apocalypse.",,,,,
emptyClassicDogHouse,items,Tool,Dog House (Empty),,,,,
emptyClassicDogHouseDesc,items,Tool,"This dog house can be placed outside your base and with a little dog food you should attract a brand new pet. But make sure to keep them safe. They are not equipped to handle this apocalypse.",,,,,
dogFoodRH,items,Tool,Dog Food,,,,,
dogFoodRHDesc,items,Tool,"This special formula dog food can be used with a dog cage or house to attract a brand new pet.",,,,,
