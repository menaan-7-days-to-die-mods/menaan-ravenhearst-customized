﻿using System;
using Harmony;
using System.Reflection;
using UnityEngine;
using DMT;

public class RH_FixMaximumItemQuality
{
    public class RH_FixMaximumItemQuality_Init : IHarmony
    {
        public void Start()
        {
            Debug.Log(" Loading Patch : " + GetType().ToString());
            var harmony = HarmonyInstance.Create(GetType().ToString());
            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }
    }

    //PassiveEffects _passiveEffect
    //ItemValue _originalItemValue = null
    //float _originalValue = 0f
    //EntityAlive _entity = null
    //Recipe _recipe = null
    //FastTags tags = default(FastTags)
    //bool calcEquipment = true
    //bool calcHoldingItem = true
    //bool calcProgression = true
    //bool calcBuffs = true
    //int craftingTier = 1
    //bool useMods = true

    [HarmonyPatch(typeof(EffectManager))]
    [HarmonyPatch("GetValue")]
    [HarmonyPatch(new Type[] { typeof(PassiveEffects), typeof(ItemValue), typeof(float), typeof(EntityAlive), typeof(Recipe), typeof(FastTags), typeof(bool), typeof(bool), typeof(bool), typeof(bool), typeof(int), typeof(bool) })]
    public class PatchEffectManagerGetValue
    {
        static void Postfix(ref float __result, ref PassiveEffects _passiveEffect, ref ItemValue _originalItemValue, ref float _originalValue, ref EntityAlive _entity, ref Recipe _recipe, ref FastTags tags, ref bool calcEquipment, ref bool calcHoldingItem, ref bool calcProgression, ref bool calcBuffs, ref int craftingTier, ref bool useMods)
        {
            if(_passiveEffect == PassiveEffects.CraftingTier)
            {
                __result = Math.Min(__result, 120);
            }
        }
    }
}

