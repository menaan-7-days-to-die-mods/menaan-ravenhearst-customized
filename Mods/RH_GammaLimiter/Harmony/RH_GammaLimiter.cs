﻿using Harmony;
using UnityEngine;
using System.Reflection;
using DMT;

public class RH_GammaLimiter
{
    public class RH_GammaLimiter_Init : IHarmony
    {
        public void Start()
        {
            Debug.Log(" Loading Patch : " + GetType().ToString());
            var harmony = HarmonyInstance.Create(GetType().ToString());
            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }
    }

    [HarmonyPatch(typeof(XUiC_OptionsVideo))]
    [HarmonyPatch("applyChanges")]
    class PatchXUiC_OptionsVideoapplyChanges
    {
        static void Postfix(XUiC_OptionsVideo __instance)
        {
            float gamma = Mathf.Min((float)__instance.comboGamma.Value, 0.60f);
            GamePrefs.Set(EnumGamePrefs.OptionsGamma, gamma);
        }
    }
}
