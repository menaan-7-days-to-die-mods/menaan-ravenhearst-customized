﻿using Harmony;
using System.Reflection;
using UnityEngine;
using DMT;
using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;

namespace RH_VehiclesTriggerPressurePlates
{
    public class RH_VehiclesTriggerPressurePlates_Init : IHarmony
    {
        public void Start()
        {
            Debug.Log(" Loading Patch : " + GetType().ToString());
            var harmony = HarmonyInstance.Create(GetType().ToString());
            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }
    }

    [HarmonyPatch(typeof(EntityVehicle))]
    [HarmonyPatch("PhysicsFixedUpdate")]
    public class PatchEntityVehiclePhysicsFixedUpdate
    {
        static void Postfix(EntityVehicle __instance)
        {
            var entityPlayer = __instance.GetAttachedPlayerLocal();
            if (entityPlayer != null)
            {
                __instance.world.CheckEntityCollisionWithBlocks(entityPlayer);
            }
        }
    }
}
